
<?php
/* @var $this SgeInscreveController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sge Inscreves',
);

$this->menu=array(
	array('label'=>'Create SgeInscreve', 'url'=>array('create')),
	array('label'=>'Manage SgeInscreve', 'url'=>array('admin')),
);
?>


<?php  
  $baseUrl = Yii::app()->theme->baseUrl; 
  $cs = Yii::app()->getClientScript();
  $cs->registerScriptFile('http://www.google.com/jsapi');
  $cs->registerCoreScript('jquery');
  $cs->registerScriptFile($baseUrl.'/js/jquery.gvChart-1.0.1.min.js');
  $cs->registerScriptFile($baseUrl.'/js/pbs.init.js');
  $cs->registerCssFile($baseUrl.'/css/jquery.css');

?>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/icons.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tables.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/mbmenu.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/mbmenu_iestyles.css" />
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bluestork/css/highcontrast.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bluestork/css/ie7.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bluestork/css/ie8.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bluestork/css/template.css" />

	

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bluestork/css/textbig.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bluestork/css/theme.css" />
	


        <link rel="stylesheet" type="text/css" href="/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="/css/application.css">
        <link type="text/css" rel="stylesheet" href="/css/bluestork/css/template_1.css">
        <link type="text/css" rel="stylesheet" href="/css/bluestork/css/template_css.css">
        <script src="/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/js/script.js"></script>
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="/js/jquery.validate.js"></script>
        <script type="text/javascript" src="/js/application.js"></script>

        <link href="/css/smoothness/jquery-ui-1.10.1.custom.css" rel="stylesheet">
</head>
  
  
	<div class="cpanel" ">
		<div class="pagetitle"><h2>Eventos</h2></div>
			<div class="icon-wrapper">
				<div class="icon">
					<a href="http://localhost/sge/index.php?r=sgeevento/create">
					
						<img src="<?php echo Yii::app()->theme->baseUrl ; ?>/images/big_icons/icon-48-user-add.png" alt=""height="42" width="42">
							<span>Novo Evento</span>
					</a>
				</div>
			</div>

			<div class="icon-wrapper">
				<div class="icon">
					<a href="http://localhost/sge/index.php?r=sgeevento/admin">
					
						<img src="<?php echo Yii::app()->theme->baseUrl ; ?>/images/big_icons/listar.png" alt=""height="42" width="42">
							<span>Listar Eventos</span>
					</a>
				</div>
			</div>
			<div class="icon-wrapper">
				<div class="icon">
					<a href="http://localhost/sge/index.php?r=sgeInscreve/index&view">
						<img src="<?php echo Yii::app()->theme->baseUrl ; ?>/images/big_icons/certificado.png" alt=""height="42" width="42">
							<span>Participando</span>
					</a>
				</div>
			</div>
			<div class="icon-wrapper">
				<div class="icon">
					<a href="http://localhost/sge/index.php?r=sgeEventoCoordena/admin&view">
						<img src="<?php echo Yii::app()->theme->baseUrl ; ?>/images/big_icons/gr.png" alt=""height="42" width="42">
							<span>Coordenador</span>
					</a>
				</div>
			</div>

	</div>





